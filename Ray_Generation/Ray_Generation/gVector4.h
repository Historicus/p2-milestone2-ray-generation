//Base code written by Jan Allbeck, Chris Czyzewicz, and Cory Boatright
//University of Pennsylvania

//modified January 25, 2017 at Grove City College

#pragma once

class gVector4{
private:
	float data[4];
public:
	///----------------------------------------------------------------------
	/// Constructors
	///----------------------------------------------------------------------
	gVector4();
	gVector4(float x, float y, float z, float w);
	
	///----------------------------------------------------------------------
	/// Getters/Setters
	///----------------------------------------------------------------------		
	/// Returns the value at index
	float operator[](unsigned int index) const;
	
	/// Returns a reference to the value at index
	float& operator[](unsigned int index);

	/// Prints the vector to standard output in a nice format
	void print();
	
	///----------------------------------------------------------------------
	/// Vector Operations
	///----------------------------------------------------------------------
	/// Returns the geometric length of the vector
	float length() const;
		
	///----------------------------------------------------------------------
	/// Operator Functions
	///----------------------------------------------------------------------		
	/// Checks if v1 == v2
	bool operator==(const gVector4& v2);
	
	/// Checks if v1 != v2
	bool operator!=(const gVector4& v2);
	
	/// Vector Addition (v1 + v2)
	gVector4 operator+(const gVector4& v2);
	
	/// Vector Subtraction (v1 - v2)
	gVector4 operator-(const gVector4& v2);
	
	/// Scalar Multiplication (v * c)
	gVector4 operator*(float c);	
	
	/// Scalar Multiplication (c * v)
	friend gVector4 operator*(float c, const gVector4& v);
	
	/// Scalar Division (v/c)
	gVector4 operator/(float c);
	
	/// Dot Product (v1 * v2)
	float operator*(const gVector4& v2);
	
	/// Cross Product (v1 % v2)
	gVector4 operator%(const gVector4& v2);
	
	/// Component-wise Product (v1 ^ v2) -- note, because of operator precedence, always use parentheses with this operator
	/// example: v3 = c * (v1 ^ v2)
	gVector4 operator^(const gVector4& v2);
};