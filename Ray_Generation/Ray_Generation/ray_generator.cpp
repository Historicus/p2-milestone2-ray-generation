/**********************************************
* Author: Samuel Kenney 2/16/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements Ray Generation class
*
**********************************************/

#include "ray_generator.h"

//constructor
ray_generator::ray_generator(std::string filename, int width, int height, gVector4 eyep, gVector4 nvec, gVector4 uvec, float fovy){
	BMPfilename = filename;
	eyePos = eyep;
	vdir = nvec;
	upvec = uvec;
	phi = fovy/2;
	this->width = (float)width;
	this->height = (float)height;
}

//destructor
ray_generator::~ray_generator(void){
}

//returns First Basis Vector
void ray_generator::FirstBasisVector(void){
	gVector4 firstBasis;
	float magnitude;
	//find the unit vector for n to return n hat
	magnitude = std::sqrt( ( vdir[0] * vdir[0] ) + ( vdir[1] * vdir[1] ) + ( vdir[2] * vdir[2] ) );
	//unit vector n
	nHat[0] = vdir[0]/magnitude;
	nHat[1] = vdir[1]/magnitude;
	nHat[2] = vdir[2]/magnitude;

	//if, when crossing, a value is a neg 0
	for (int i = 0; i < 3; i++)
	{
		if (nHat[i] == 0){
			nHat[i] = std::abs(nHat[i]);
		}

	}

	std::cout << "nHat ";
	nHat.print();
}

//returns Second Basis Vector
void ray_generator::SecondBasisVector(void){
	float magnitude;

	//cross product of nhat x upvec to get uvec. Then find magnitude and convert to unit vector
	uvec = nHat % upvec;

	magnitude = std::sqrt( ( uvec[0] * uvec[0] ) + ( uvec[1] * uvec[1] ) + ( uvec[2] * uvec[2] ) );
	//unit vector u
	uHat[0] = uvec[0]/magnitude;
	uHat[1] = uvec[1]/magnitude;
	uHat[2] = uvec[2]/magnitude;

	//if, when crossing, a value is a neg 0
	for (int i = 0; i < 3; i++)
	{
		if (uHat[i] == 0){
			uHat[i] = std::abs(uHat[i]);
		}

	}

	std::cout << "uHat ";
	uHat.print();
}

//returns Third Basis Vector
void ray_generator::ThirdBasisVector(void){
	float magnitude;

	//cross product of uhat x nhat to get up. Then find magnitude and convert to unit vector
	upvec = uHat % nHat;

	magnitude = std::sqrt( ( upvec[0] * upvec[0] ) + ( upvec[1] * upvec[1] ) + ( upvec[2] * upvec[2] ) );
	//unit vector up
	upHat[0] = upvec[0]/magnitude;
	upHat[1] = upvec[1]/magnitude;
	upHat[2] = upvec[2]/magnitude;

	//if, when crossing, a value is a neg 0
	for (int i = 0; i < 3; i++)
	{
		if (upHat[i] == 0){
			upHat[i] = std::abs(upHat[i]);
		}

	}

	std::cout << "upHat ";
	upHat.print();

}

//calculate M, V, and H
void ray_generator::calculateMVH(void){
	//convert from degrees to radians
	M = eyePos + nHat;
	V = upHat * (std::tanf( phi * (PI / 180 )));
	//tan(theta) = tan(phi) * aspect
	H = uHat * (std::tanf( phi * (PI / 180 )) * (this->width/this->height));
	for (int i = 0; i < 3; i++)
	{
		if (M[i] == 0){
			M[i] = std::abs(M[i]);
		}
		if (V[i] == 0){
			V[i] = std::abs(V[i]);
		}
		if (H[i] == 0){
			H[i] = std::abs(H[i]);
		}

	}
	std::cout << "M: ";
	M.print();
	std::cout << "V: ";
	V.print();
	std::cout << "H: ";
	H.print();
}


//generates ray with given x and y values- the rest are in class
gVector4 ray_generator::generateRay(int x1, int y1){
	gVector4 p;
	gVector4 rayDirection;
	gVector4 ray;
	float x = (float)x1, y = (float)y1;
	float magnitude;

	//get first half
	float first = 2 * x;
	float sec = (width - 1 );
	float third = (first / sec) -1;
	gVector4 firstHalf = H * third;

	//get second half
	float first2 = 2 * y;
	float sec2  =(height - 1 );
	float third2 = (first2 / sec2) -1;
	gVector4 secondHalf = V * third2;

	//find position of the center of the pixel
	p = M +  firstHalf + secondHalf; 
	//find ray direction
	rayDirection = p - eyePos;

	//normalize
	magnitude = std::sqrt( ( rayDirection[0] * rayDirection[0] ) + ( rayDirection[1] * rayDirection[1] ) + ( rayDirection[2] * rayDirection[2] ) );
	ray[0] = rayDirection[0]/magnitude;
	ray[1] = rayDirection[1]/magnitude;
	ray[2] = rayDirection[2]/magnitude; 

	//make sure its absolute value
	for (int i = 0; i < 3; i++)
	{
		ray[i] *= 255;

	}
	//ray.print();
	return ray;
}

//factory for creating ray_generator with values from file- most of computation for vectors will be done here
ray_generator* ray_generator::factory(const std::string& filename){

	//new ray_generator
	ray_generator *new_ray_generator;

	//used to holding strings from text
	std::string value;
	//used when parsing text file with multiple numebrs
	std::string numInTxt;

	//stream for text file
	std::ifstream rayFile (filename);

	//get filename
	std::string fileBMP = "";
		//reso
		int fakewidth = 0, fakeheight = 0;
		//eyep
	gVector4 eyep(0.0,0.0,0.0,0.0);
		//vdir
	gVector4 vdir(0.0,0.0,0.0,0.0);
		//uvec
	gVector4 uvec(0.0,0.0,0.0,0.0);
		//fovy
	float fieldofV = 0.0f;

	//Index for place in text file
	int in = 0;
	while (getline(rayFile, value)){
		//remove beginning text
		value.erase(0,5);
		if (in == 0){
			fileBMP = value;
			std::cout << "FILE " << fileBMP << std::endl;

		} else if ( in == 1) {
			//grab first value of res
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store first value
			fakewidth = std::stoi(numInTxt);

			//grab second value of res
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store second value
			fakeheight = std::stoi(numInTxt);

			std::cout << "RES " << fakewidth << " " << fakeheight << std::endl;

		} else if ( in == 2){
			//grab x value of eyep
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store x value
			eyep[0] = std::stof(numInTxt);

			//grab y value of eyep
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store y value
			eyep[1] = std::stof(numInTxt);

			//grab z value of eyep
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store z value
			eyep[2] = std::stof(numInTxt);

			std::cout << "EYEP " << eyep[0] << " " << eyep[1] << " " << eyep[2] << std::endl;

		}else if ( in == 3){
			//grab x value of vdir
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store x value
			vdir[0] = std::stof(numInTxt);

			//grab y value of vdir
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store y value
			vdir[1] = std::stof(numInTxt);

			//grab z value of vdir
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store z value
			vdir[2] = std::stof(numInTxt);

			std::cout << "VDIR " << vdir[0] << " " << vdir[1] << " " << vdir[2] << std::endl;

		}else if ( in == 4){
			//grab x value of uvec
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store x value
			uvec[0] = std::stof(numInTxt);

			//grab y value of uvec
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store y value
			uvec[1] = std::stof(numInTxt);

			//grab z value of uvec
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store z value
			uvec[2] = std::stof(numInTxt);

			std::cout << "UVEC " << uvec[0] << " " << uvec[1] << " " << uvec[2] << std::endl;
		}else if ( in == 5){
			//grab FOVY value
			numInTxt = value.substr(0, value.find(" "));
			value.erase(0, value.find(" ")+1);
			//store FOVY value
			fieldofV = std::stof(numInTxt);
			std::cout << "FOVY " << fieldofV << std::endl;
		}

		in++;
	}
	new_ray_generator = new ray_generator(fileBMP, fakewidth, fakeheight, eyep, vdir, uvec, fieldofV);

	rayFile.close();

	return new_ray_generator;
}
