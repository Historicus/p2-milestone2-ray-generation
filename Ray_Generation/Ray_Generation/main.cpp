/**
 * An example program that creates a 24-bit bitmap file that is 800 x 600 and makes a blue/red checkerboard pattern.
 * Uses EasyBMP
 *
 * Cory Boatright
 * University of Pennsylvania, Fall 2011
 * AND Grove City College 2015
 **/

#include "EasyBMP.h"
#include "ray_generator.h"

using namespace std;

ray_generator *test1;
ray_generator *test2;
ray_generator *test3;

int main(int argc, char** argv) {

	cout << "***** Constructing Test 1 Ray Generation *****" << endl;
	test1 = ray_generator::factory("test1.txt");

	test1->FirstBasisVector();
	test1->SecondBasisVector();
	test1->ThirdBasisVector();
	test1->calculateMVH();

	BMP output1;
	output1.SetSize(test1->width, test1->height);
	output1.SetBitDepth(24);
	gVector4 ray1;
	
	for(unsigned int x = 0; x < test1->width; x++) {
		for(unsigned int y = 0; y < test1->height; y++) {
			ray1 = test1->generateRay(x, y);
			output1(x, y)->Red = std::abs(ray1[0]);
			output1(x, y)->Blue = std::abs(ray1[2]);
			output1(x, y)->Green = std::abs(ray1[1]);
		}
	}

	output1.WriteToFile(test1->BMPfilename.c_str());

	cout << "***** Constructing Test 2 Ray Generation *****" << endl;
	test2 = ray_generator::factory("test2.txt");

	test2->FirstBasisVector();
	test2->SecondBasisVector();
	test2->ThirdBasisVector();
	test2->calculateMVH();

	BMP output2;
	output2.SetSize(test2->width, test2->height);
	output2.SetBitDepth(24);
	gVector4 ray2;
	
	for(unsigned int x = 0; x < test2->width; x++) {
		for(unsigned int y = 0; y < test2->height; y++) {
			ray2 = test2->generateRay(x, y);
			output2(x, y)->Red = std::abs(ray2[0]);
			output2(x, y)->Blue = std::abs(ray2[2]);
			output2(x, y)->Green = std::abs(ray2[1]);
		}
	}

	output2.WriteToFile(test2->BMPfilename.c_str());

	cout << "***** Constructing Test 3 Ray Generation *****" << endl;
	test3 = ray_generator::factory("test3.txt");

	test3->FirstBasisVector();
	test3->SecondBasisVector();
	test3->ThirdBasisVector();
	test3->calculateMVH();

	BMP output3;
	output3.SetSize(test3->width, test3->height);
	output3.SetBitDepth(24);
	gVector4 ray3;
	
	for(unsigned int x = 0; x < test3->width; x++) {
		for(unsigned int y = 0; y < test3->height; y++) {
			ray3 = test3->generateRay(x, y);
			output3(x, y)->Red = std::abs(ray3[0]);
			output3(x, y)->Blue = std::abs(ray3[2]);
			output3(x, y)->Green = std::abs(ray3[1]);
		}
	}

	output3.WriteToFile(test3->BMPfilename.c_str());
	return 0;
}