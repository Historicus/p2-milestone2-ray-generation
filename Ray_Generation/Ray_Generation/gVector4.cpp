/**********************************************
* Author: Samuel Kenney 2/1/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements Vector Operation Library class
*
**********************************************/
#include "gVector4.h"
#include <iostream>
#include <cmath>

///----------------------------------------------------------------------
/// Constructors
///----------------------------------------------------------------------
gVector4::gVector4(){
	data[0] = 0.0f;
	data[1] = 0.0f;
	data[2] = 0.0f;
	data[3] = 0.0f;
}
gVector4::gVector4(float x, float y, float z, float w){
	data[0] = x;
	data[1] = y;
	data[2] = z;
	data[3] = w;
}

///----------------------------------------------------------------------
/// Getters/Setters
///----------------------------------------------------------------------		
/// Returns the value at index
float gVector4::operator[](unsigned int index) const{
	return data[index];
}

/// Returns a reference to the value at index
float& gVector4::operator[](unsigned int index){
	return data[index];
}

/// Prints the vector to standard output in a nice format
void gVector4::print(){
	std::cout << "[ " << data[0] << " , " << data[1] << " , " << data [2] << " , " << data[3] << " ]" << std::endl;
}

///----------------------------------------------------------------------
/// Vector Operations
///----------------------------------------------------------------------
/// Returns the geometric length of the vector
float gVector4::length() const{
	return std::sqrt( ( data[0] * data[0] ) + ( data[1] * data[1] ) + ( data[2] * data[2] ) );
}

///----------------------------------------------------------------------
/// Operator Functions
///----------------------------------------------------------------------		
/// Checks if v1 == v2
bool gVector4::operator==(const gVector4& v2){
	int equal = 0; 
	float epsilon = 0.0001f;
	if (data[0] == 0 || v2.data[0] == 0){
		if (std::abs(data[0] - v2.data[0]) < epsilon){equal++;}
	} else if ( ( std::abs(data[0] - v2.data[0] ) / ( std::abs ( data[0] ) + abs( v2.data[0] ) ) ) < epsilon ) {equal++;}

	if (data[1] == 0 || v2.data[1] == 0){
		if (std::abs(data[1] - v2.data[1]) < epsilon){equal++;}
	} else if ( ( std::abs(data[1] - v2.data[1] ) / ( std::abs ( data[1] ) + abs( v2.data[1] ) ) ) < epsilon ){equal++;}

	if (data[2] == 0 || v2.data[2] == 0){
		if (std::abs(data[2] - v2.data[2]) < epsilon){equal++;}
	} else if ( ( std::abs(data[2] - v2.data[2] ) / ( std::abs ( data[2] ) + abs( v2.data[2] ) ) ) < epsilon ){equal++;}

	if (data[3] == 0 || v2.data[3] == 0){
		if (std::abs(data[3] - v2.data[3]) < epsilon){equal++;}
	} else if ( ( std::abs(data[3] - v2.data[3] ) / ( std::abs ( data[3] ) + abs( v2.data[3] ) ) ) < epsilon ){equal++;}
	
	if (equal == 4){return true;}
	else {return false;}
}

/// Checks if v1 != v2
bool gVector4::operator!=(const gVector4& v2){
	int equal = 4; 
	float epsilon = 0.0001f;

	if (data[0] == 0 || v2.data[0] == 0){
		if (std::abs(data[0] - v2.data[0]) > epsilon){equal--;}
	} else if ( ( std::abs(data[0] - v2.data[0] ) / ( std::abs ( data[0] ) + abs( v2.data[0] ) ) ) > epsilon ) {equal--;}

	if (data[1] == 0 || v2.data[1] == 0){
		if (std::abs(data[1] - v2.data[1]) > epsilon){equal--;}
	} else if ( ( std::abs(data[1] - v2.data[1] ) / ( std::abs ( data[1] ) + abs( v2.data[1] ) ) ) > epsilon ){equal--;}

	if (data[2] == 0 || v2.data[2] == 0){
		if (std::abs(data[2] - v2.data[2]) > epsilon){equal--;}
	} else if ( ( std::abs(data[2] - v2.data[2] ) / ( std::abs ( data[2] ) + abs( v2.data[2] ) ) ) > epsilon ){equal--;}

	if (data[3] == 0 || v2.data[3] == 0){
		if (std::abs(data[3] - v2.data[3]) > epsilon){equal--;}
	} else if ( ( std::abs(data[3] - v2.data[3] ) / ( std::abs ( data[3] ) + abs( v2.data[3] ) ) ) > epsilon ){equal--;}

	if (equal < 4){return true;}
	else {return false;} 
}

/// Vector Addition (v1 + v2)
gVector4 gVector4::operator+(const gVector4& v2){
	gVector4 add;
	add[0] = data[0] + v2.data[0];
	add[1] = data[1] + v2.data[1];
	add[2] = data[2] + v2.data[2];

	//if point, stays a point. If vector, stays a vector
	if ( (data[3] + v2.data[3]) != 0 ){
		add[3] = 1;
	} else {
		add[3] = 0;
	}

	return add;
}

/// Vector Subtraction (v1 - v2)
gVector4 gVector4::operator-(const gVector4& v2){
	gVector4 sub;
	sub[0] = data[0] - v2.data[0];
	sub[1] = data[1] - v2.data[1];
	sub[2] = data[2] - v2.data[2];
	
	//if point, stays a point. If vector, stays a vector
	if ( (data[3] + v2.data[3]) != 0 ){
		sub[3] = 1;
	} else {
		sub[3] = 0;
	}

	return sub;
}

/// Scalar Multiplication (v * c)
gVector4 gVector4::operator*(float c){
	gVector4 multVC;
	multVC[0] = data[0] * c;
	multVC[1] = data[1] * c;
	multVC[2] = data[2] * c;
	//keep w the same
	multVC[3] = data[3];


	return multVC;
}	

//friend gVector4 operator*(float c, const gVector4& v);
gVector4 operator*(float c, const gVector4& v){
	gVector4 multCV;
	multCV[0] = c * v.data[0];
	multCV[1] = c * v.data[1];
	multCV[2] = c * v.data[2];
	//keep w the same
	multCV[3] = v.data[3];

	return multCV;
}


/// Scalar Division (v/c)
gVector4 gVector4::operator/(float c){
	gVector4 div;
	div[0] = data[0] / c;
	div[1] = data[1] / c;
	div[2] = data[2] / c;
	//keep w the same
	div[3] = data[3];

	return div;
}

/// Dot Product (v1 * v2)
float gVector4::operator*(const gVector4& v2){
	return ( ( data[0] * v2.data[0] ) + ( data[1] * v2.data[1] ) + ( data[2] * v2.data[2] ) + ( data[3] * v2.data[3] ) );
}

/// Cross Product (v1 % v2)
gVector4 gVector4::operator%(const gVector4& v2){
	gVector4 cross;
	cross[0] = ( ( v2.data[2] * data[1] ) - ( v2.data[1] * data[2] ) );
	cross[1] = ( ( v2.data[0] * data[2] ) - ( v2.data[2] * data[0] ) );
	cross[2] = ( ( v2.data[1] * data[0] ) - ( v2.data[0] * data[1] ) );

	return cross;
}

/// Component-wise Product (v1 ^ v2) -- note, because of operator precedence, always use parentheses with this operator
/// example: v3 = c * (v1 ^ v2)
gVector4 gVector4::operator^(const gVector4& v2){
	gVector4 compWise;
	compWise[0] = ( data[0] * v2.data[0] );
	compWise[1] = ( data[1] * v2.data[1] );
	compWise[2] = ( data[2] * v2.data[2] );
	//compWise[3] = ( data[3] * v2.data[3] );
	if ( (data[3] + v2.data[3]) != 0 ){
		compWise[3] = 1;
	} else {
		compWise[3] = 0;
	}

	return compWise;
}